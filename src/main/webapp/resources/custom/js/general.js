$(document).ready(function() {

	$('.subNav').hide();
	$('.mainNav li').hover(function() {
		$(this).children('a').addClass('active');
		$(this).find('.subNav').show();
	}, function() {
		$(this).children('a').removeClass('active');
		$(this).find('.subNav').hide();
	});

});

function setActive(id){
	$(".sidebar-menu li").removeClass("active");
	$("#" + id).parent().show();
	$("#" + id).parent().addClass("menu-open");
	$("#" + id).parent().parent().addClass("active");
	$("#" + id).addClass("active");
}

function filtersearch(tblid, object, colindex){
	$("#" + tblid).dataTable().fnFilter("^" + $(object).val(),colindex,true,true);
}

$.urlParam = function(name) {
	var results = new RegExp('[\\?&]' + name + '=([^&#]*)')
			.exec(window.location.href);
	if(results != null){
		return results[1];
	}
	return null;
};

$('#frmUser').ajaxForm({
	dataType : 'json',
	success : function(jsonData) {
		$.unblockUI({
			onUnblock : function() {
				vex.dialog.alert({
					message : jsonData.message,
					callback : function(value) {
						if (jsonData.success) {
							if(jsonData.redirectUrl)
							window.location.href = jsonData.redirectUrl;
						}
					}				
				});
			}
		});
	}
});

$('#frmchangepassword').ajaxForm({
	dataType : 'json',
	success : function(jsonData) {
		$.unblockUI({
			onUnblock : function() {
				vex.dialog.alert({
					message : jsonData.message,
					callback : function(value) {
						if (jsonData.success) {
							parent.jQuery.fancybox.close();
						}
					}				
				});
			}
		});
	}
});

$('#frmforgetpassword').ajaxForm({
	dataType : 'json',
	success : function(jsonData) {
		$.unblockUI({
			onUnblock : function() {
				vex.dialog.alert({
					message : jsonData.message,
					callback : function(value) {
						if (jsonData.success) {
							parent.jQuery.fancybox.close();
						}
					}				
				});
			}
		});
	}
});

