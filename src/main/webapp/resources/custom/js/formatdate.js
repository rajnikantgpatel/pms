function getFormattedDate(strDate) {
	var parsedDate = "";

	if (strDate != null && strDate != "null" && strDate != "") {

		var fromDate = new Date(strDate);

		var fromMonth = fromDate.getMonth() + 1;
		var fromDay = fromDate.getDate();

		if (fromDay.toString().length == 1) {
			fromDay = "0" + fromDay;
		}

		if (fromMonth.toString().length == 1) {
			fromMonth = "0" + fromMonth;
		}

		parsedDate = fromDay + "/" + fromMonth + "/" + fromDate.getFullYear();
	}
	
	return parsedDate;
}

function getFormattedDateTime(strDate) {
	var parsedDate = "";

	if (strDate != null && strDate != "null" && strDate != "") {

		var fromDate = new Date(strDate);

		var fromMonth = fromDate.getMonth() + 1;
		var fromDay = fromDate.getDate();
		var fromHours = fromDate.getHours();
		var fromAmPm = "AM";
		var fromMinutes = fromDate.getMinutes();

		if (fromDay.toString().length == 1) {
			fromDay = "0" + fromDay;
		}

		if (fromMonth.toString().length == 1) {
			fromMonth = "0" + fromMonth;
		}

		if (fromHours > 12) {
			fromHours = fromHours - 12;
			fromAmPm = "PM";
		}

		if (fromHours.toString().length == 1) {
			fromHours = "0" + fromHours;
		}

		if (fromMinutes.toString().length == 1) {
			fromMinutes = "0" + fromMinutes;
		}

		parsedDate = fromDay + "/" + fromMonth + "/" + fromDate.getFullYear()
				+ " " + fromHours + ":" + fromMinutes + " " + fromAmPm;
	}
	
	return parsedDate;
}