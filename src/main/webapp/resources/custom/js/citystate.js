
var states = new Array("","Andaman and Nicobar Island", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh","Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana", "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal");


var city = new Array();
city[0]="";
city[1]="|Nicobar|North and Middle Andaman|South Andaman";
city[2]="|Anantapur|Chittoor|Cuddapah|East Godavari|Guntur|Krishna|Kurnool|Nellore|Prakasam|Srikakulam|Visakhapatnam|Vizianagaram|West Godavari";
city[3]="|Anjaw|Changlang|Dibang Valley|East Kameng|East Siang|Kurung Kumey|Lohit|Longding|Lower Dibang Valley|Lower Subansiri|Papum Pare|Tawang|Tirap|Upper Siang|Upper Subansiri|West Kameng|West Siang";
city[4]="|Baksa|Barpeta|Bongaigaon|Cachar|Chirang|Darrang|Dhemaji|Dhubri|Dibrugarh|Dima Hasao|Goalpara|Golaghat|Hailakandi|Jorhat|Kamrup Metropolitan|Kamrup|Karbi Anglong|Karimganj|Kokrajhar|Lakhimpur|Morigaon|Nagaon|Nalbari|Sivasagar|Sonitpur|Tinsukia|Udalguri";
city[5]="|Araria|Arwal|Aurangabad|Banka|Begusarai|Bhagalpur|Bhojpur|Buxar|Darbhanga|East Champaran (Motihari)|Gaya|Gopalganj|Jamui|Jehanabad|Kaimur (Bhabua)|Katihar|Khagaria|Kishanganj|Lakhisarai|Madhepura|Madhubani|Munger (Monghyr)|Muzaffarpur|Nalanda|Nawada|Patna|Purnia (Purnea)|Rohtas|Saharsa|Samastipur|Saran|Sheikhpura|Sheohar|Sitamarhi|Siwan|Supaul|Vaishali|West Champaran";
city[6]="|Chandigarh";
city[7]="|Balod|Baloda Bazar|Balrampur|Bastar|Bemetara|Bijapur|Bilaspur|Dantewada (South Bastar)|Dhamtari|Durg|Gariaband|Janjgir-Champa|Jashpur|Kabirdham (Kawardha)|Kanker (North Bastar)|Kondagaon|Korba|Korea (Koriya)|Mahasamund|Mungeli|Narayanpur|Raigarh|Raipur|Rajnandgaon|Sukma|Surajpur|Surguja";
city[8]="|Dadra & Nagar Haveli";
city[9]="|Daman|Diu";
city[10]="|Central Delhi|East Delhi|New Delhi|North Delhi|North East Delhi|North West Delhi|South Delhi|South West Delhi|West Delhi";
city[11]="|North Goa|South Goa";
city[12]="|Ahmedabad|Amreli|Anand|Aravalli|Banaskantha (Palanpur)|Bharuch|Bhavnagar|Botad|Chhota Udepur|Dahod|Dangs (Ahwa)|Devbhoomi Dwarka|Gandhinagar|Gir Somnath|Jamnagar|Junagadh|Kachchh|Kheda (Nadiad)|Mahisagar|Mehsana|Morbi|Narmada (Rajpipla)|Navsari|Panchmahal (Godhra)|Patan|Porbandar|Rajkot|Sabarkantha (Himmatnagar)|Surat|Surendranagar|Tapi (Vyara)|Vadodara|Valsad";
city[13]="|Ambala|Bhiwani|Faridabad|Fatehabad|Gurgaon|Hisar|Jhajjar|Jind|Kaithal|Karnal|Kurukshetra|Mahendragarh|Mewat|Palwal|Panchkula|Panipat|Rewari|Rohtak|Sirsa|Sonipat|Yamunanagar";
city[14]="|Bilaspur|Chamba|Hamirpur|Kangra|Kinnaur|Kullu|Lahaul & Spiti|Mandi|Shimla|Sirmaur (Sirmour)|Solan|Una";
city[15]="|Anantnag|Bandipora|Baramulla|Budgam|Doda|Ganderbal|Jammu|Kargil|Kathua|Kishtwar|Kulgam|Kupwara|Leh|Poonch|Pulwama|Rajouri|Ramban|Reasi|Samba|Shopian|Srinagar|Udhampur";
city[16]="|Bokaro|Chatra|Deoghar|Dhanbad|Dumka|East Singhbhum|Garhwa|Giridih|Godda|Gumla|Hazaribag|Jamtara|Khunti|Koderma|Latehar|Lohardaga|Pakur|Palamu|Ramgarh|Ranchi|Sahibganj|Seraikela-Kharsawan|Simdega|West Singhbhum";
city[17]="|Bagalkot|Bangalore Rural|Bangalore Urban|Belgaum|Bellary|Bidar|Bijapur|Chamarajanagar|Chickmagalur|Chikballapur|Chitradurga|Dakshina Kannada|Davangere|Dharwad|Gadag|Gulbarga|Hassan|Haveri|Kodagu|Kolar|Koppal|Mandya|Mysore|Raichur|Ramnagara|Shimoga|Tumkur|Udupi|Uttara Kannada (Karwar|Yadgir";
city[18]="|Alappuzha|Ernakulam|Idukki|Kannur|Kasaragod|Kollam|Kottayam|Kozhikode|Malappuram|Palakkad|Pathanamthitta|Thiruvananthapuram|Thrissur|Wayanad";
city[19]="|Lakshadweep";
city[20]="|Alirajpur|Anuppur|Ashoknagar|Balaghat|Barwani|Betul|Bhind|Bhopal|Burhanpur|Chhatarpur|Chhindwara|Damoh|Datia|Dewas|Dhar|Dindori|Guna|Gwalior|Harda|Hoshangabad|Indore|Jabalpur|Jhabua|Katni|Khandwa|Khargone|Mandla|Mandsaur|Morena|Narsinghpur|Neemuch|Panna|Raisen|Rajgarh|Ratlam|Rewa|Sagar|Satna|Sehore|Seoni|Shahdol|Shajapur|Sheopur|Shivpuri|Sidhi|Singrauli|Tikamgarh|Ujjain|Umaria|Vidisha";
city[21]="|Ahmednagar|Akola|Amravati|Aurangabad|Beed|Bhandara|Buldhana|Chandrapur|Dhule|Gadchiroli|Gondia|Hingoli|Jalgaon|Jalna|Kolhapur|Latur|Mumbai City|Mumbai Suburban|Nagpur|Nanded|Nandurbar|Nashik|Osmanabad|Parbhani|Pune|Raigad|Ratnagiri|Sangli|Satara|Sindhudurg|Solapur|Thane|Wardha|Washim|Yavatmal";
city[22]="|Bishnupur|Chandel|Churachandpur|Imphal East|Imphal West|Senapati|Tamenglong|Thoubal|Ukhrul";
city[23]="|East Garo Hills|East Jaintia Hills|East Khasi Hills|North Garo Hills|Ri Bhoi|South Garo Hills|South West Garo Hills|South West Khasi Hills|West Garo Hills|West Jaintia Hills|West Khasi Hills";
city[24]="|Aizawl|Champhai|Kolasib|Lawngtlai|Lunglei|Mamit|Saiha|Serchhip";
city[25]="|Dimapur|Kiphire|Kohima|Longleng|Mokokchung|Mon|Peren|Phek|Tuensang|Wokha|Zunheboto";
city[26]="|Angul|Balangir|Balasore|Bargarh|Bhadrak|Bhubaneswar|Boudh|Cuttack|Deogarh|Dhenkanal|Gajapati|Ganjam|Jagatsinghapur|Jajpur|Jharsuguda|Kalahandi|Kandhamal|Kendrapara|Kendujhar (Keonjhar)|Khordha|Koraput|Malkangiri|Mayurbhanj|Nabarangpur|Nayagarh|Nuapada|Puri|Rayagada|Sambalpur|Sonepur|Sundargarh";
city[27]="|Karaikal|Mahe|Pondicherry|Yanam";
city[28]="|Amritsar|Barnala|Bathinda|Faridkot|Fatehgarh Sahib|Fazilka|Ferozepur|Gurdaspur|Hoshiarpur|Jalandhar|Kapurthala|Ludhiana|Mansa|Moga|Muktsar|Nawanshahr|Pathankot|Patiala|Rupnagar|Sangrur|SAS Nagar (Mohali)|Tarn Taran";
city[29]="|Ajmer|Alwar|Banswara|Baran|Barmer|Bharatpur|Bhilwara|Bikaner|Bundi|Chittorgarh|Churu|Dausa|Dholpur|Dungarpur|Hanumangarh|Jaipur|Jaisalmer|Jalore|Jhalawar|Jhunjhunu|Jodhpur|Karauli|Kota|Nagaur|Pali|Pratapgarh|Rajsamand|Sawai Madhopur|Sikar|Sirohi|Sri Ganganagar|Tonk|Udaipur";
city[30]="|East Sikkim|North Sikkim|South Sikkim|West Sikkim";
city[31]="|Ariyalur|Chennai|Coimbatore|Cuddalore|Dharmapuri|Dindigul|Erode|Kanchipuram|Kanyakumari|Karur|Krishnagiri|Madurai|Nagapattinam|Namakkal|Nilgiris|Perambalur|Pudukkottai|Ramanathapuram|Salem|Sivaganga|Thanjavur|Theni|Thoothukudi (Tuticorin)|Tiruchirappalli|Tirunelveli|Tiruppur|Tiruvallur|Tiruvannamalai|Tiruvarur|Vellore|Viluppuram|Virudhunagar";
city[32]="|Adilabad|Hyderabad|Karimnagar|Khammam|Mahabubnagar|Medak|Nalgonda|Nizamabad|Rangareddy|Warangal";
city[33]="|Dhalai|Gomati|Khowai|North Tripura|Sepahijala|South Tripura|Unakoti|West Tripura";
city[34]="|Agra|Aligarh|Allahabad|Ambedkar Nagar|Auraiya|Azamgarh|Baghpat|Bahraich|Ballia|Balrampur|Banda|Barabanki|Bareilly|Basti|Bhim Nagar|Bijnor|Budaun|Bulandshahr|Chandauli|Chatrapati Sahuji Mahraj Nagar|Chitrakoot|Deoria|Etah|Etawah|Faizabad|Farrukhabad|Fatehpur|Firozabad|Gautam Buddha Nagar|Ghaziabad|Ghazipur|Gonda|Gorakhpur|Hamirpur|Hardoi|Hathras|Jalaun|Jaunpur|Jhansi|Jyotiba Phule Nagar (J.P. Nagar)|Kannauj|Kanpur Dehat|Kanpur Nagar|Kanshiram Nagar (Kasganj)|Kaushambi|Kushinagar (Padrauna)|Lakhimpur - Kheri|Lalitpur|Lucknow|Maharajganj|Mahoba|Mainpuri|Mathura|Mau|Meerut|Mirzapur|Moradabad|Muzaffarnagar|Panchsheel Nagar|Pilibhit|Prabuddh Nagar|Pratapgarh|RaeBareli|Rampur|Saharanpur|Sant Kabir Nagar|Sant Ravidas Nagar|Shahjahanpur|Shravasti|Siddharth Nagar|Sitapur|Sonbhadra|Sultanpur|Unnao|Varanasi";
city[35]="|Almora|Bageshwar|Chamoli|Champawat|Dehradun|Haridwar|Nainital|Pauri Garhwal|Pithoragarh|Rudraprayag|Tehri Garhwal|Udham Singh Nagar|Uttarkashi";
city[36]="|Bankura|Birbhum|Burdwan (Bardhaman)|Cooch Behar|Dakshin Dinajpur (South Dinajpur)|Darjeeling|Hooghly|Howrah|Jalpaiguri|Kolkata|Malda|Murshidabad|Nadia|North 24 Parganas|Paschim Medinipur (West Medinipur)|Purba Medinipur (East Medinipur)|Purulia|South 24 Parganas|Uttar Dinajpur (North Dinajpur)";

function print_state(){
    //given the id of the <select> tag as function argument, it inserts <option> tags
    var option_str = document.getElementById('state');
    option_str.length=0;
    option_str.options[0] = new Option('Select State','');
    option_str.selectedIndex = 0;
    for (var i=1; i<states.length; i++) {
    option_str.options[i] = new Option(states[i],states[i]);
    }
    setcompleteuncomplete('state');
}

function print_passed_state(){
    //given the id of the <select> tag as function argument, it inserts <option> tags
    var option_str = document.getElementById('passed_state');
    option_str.length=0;
    option_str.options[0] = new Option('Select State','');
    option_str.selectedIndex = 0;
    for (var i=1; i<states.length; i++) {
    option_str.options[i] = new Option(states[i],states[i]);
    }
    setcompleteuncomplete('state');
}

function print_city(selectedIndex, name,state){
	var option_str = document.getElementById('city');
    option_str.length=0;    // Fixed by Julian Woods
    option_str.options[0] = new Option('Select City','');
    option_str.selectedIndex = 0;
	
	if(state != null){
		 if(selectedIndex != 0){
			    var citystr = city[selectedIndex];
			    var city_arr = citystr.split("|");
			    for (var i=1; i<city_arr.length; i++) {
			    option_str.options[i] = new Option(city_arr[i],city_arr[i]);
			    }
		    }else{
		    	selectedIndex = states.indexOf(state);
		    	var citystr = city[selectedIndex];
		 	    var city_arr = citystr.split("|");
		 	    for (var i=1; i<city_arr.length; i++) {
		 	    option_str.options[i] = new Option(city_arr[i],city_arr[i]);
		 	    }
		    }
	}
	
    setcompleteuncomplete(name);
    setcompleteuncomplete('city');
}
    