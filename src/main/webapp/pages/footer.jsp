<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <!-- SlimScroll -->
    <script src="<c:url value = "/resources/plugins/slimScroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<c:url value = "/resources/plugins/fastclick/fastclick.min.js"/>'></script>
    <!-- AdminLTE App -->
    <script src="<c:url value = "/resources/dist/js/app.min.js"/>" type="text/javascript"></script>
    
    <!-- Demo -->
    <script src="<c:url value = "/resources/dist/js/demo.js"/>" type="text/javascript"></script>