<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
    <meta charset="UTF-8">
    <title> | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<c:url value = "/resources/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
 	 <!-- Font Awesome Icons -->
    <link href="<c:url value = "/resources/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<c:url value = "/resources/css/ionicons.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<c:url value = "/resources/css/AdminLTE.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<c:url value = "/resources/css/skins/_all-skins.min.css"/>" rel="stylesheet" type="text/css" />
	
	<link href="<c:url value = "/resources/dist/css/smart-forms.css"/>" type="text/css" rel="stylesheet" />
	 <!-- custom -->
	<link href="<c:url value = "/resources/custom/css/customstyle.css"/>" rel="stylesheet" media="screen">
	<!-- fancybox -->
	<link href="<c:url value = "/resources/custom/css/jquery.fancybox.css"/>" rel="stylesheet" media="screen">
	<!-- vex slider -->
	<link href="<c:url value = "/resources/custom/css/vex.css"/>" rel="stylesheet" type="text/css"/>
	<link href="<c:url value = "/resources/custom/css/vex-theme-default.css"/>" rel="stylesheet" type="text/css"/>		
	 
	 <!-- jQuery 2.1.4 -->
   <%--  <script src="<c:url value = "/resources/plugins/jQuery/jQuery-2.1.4.min.js"/>"></script> --%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<c:url value = "/resources/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <!-- vex slider -->
    <script type="text/javascript" src="<c:url value = "/resources/custom/js/vex.combined.min.js"/>"></script>
    <script>vex.defaultOptions.className = 'vex-theme-default';</script>
    <!-- Block UI -->
    <script type="text/javascript" src="<c:url value = "/resources/custom/js/jquery.blockUI.js" />"></script>
    <!-- fancybox -->
    <script type="text/javascript" src="<c:url value = "/resources/custom/js/jquery.fancybox.js"/>"></script>
    
     <!--  ajaxForm -->
     <script type="text/javascript" src="<c:url value = "/resources/custom/js/jquery.form.js" />"></script>    
   <!--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>  -->
      <!-- Custom -->
    <script type="text/javascript" src="<c:url value = "/resources/custom/js/general.js" />"></script> 
    <script>
	$(document).ready(function() {
		$('.fancybox').fancybox();
	});
</script>
  </head>