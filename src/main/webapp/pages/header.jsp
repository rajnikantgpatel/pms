<header class="main-header">
        <!-- Logo -->
        <a href="/PMS" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <!-- <span class="logo-mini"><b>A</b>LT</span> -->
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Perfect</b>Solusoft</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
         <!-- <div class="topText">Press Managment System</div> -->
		<div class="navbar-custom-menu">
			 <ul class="nav navbar-nav">		
				<li style="padding-top: 15px;color: white;"><span class="hidden-xs">Welcome,<em style="margin-left:5px;">${userInstance.username } </em></span></li>		
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i>					
				</a>
					<ul class="dropdown-menu">
						<!-- Menu Footer-->
						<li class="user-footer"><a href="changepassword" class="fancybox fancybox.iframe"><i
								class="fa fa-lock"></i> &nbsp;Change password</a> <a href="logout"><i
								class="fa fa-power-off"></i> &nbsp;Logout</a></li>
					</ul></li>
			</ul> 
		</div>
        </nav>
      </header>