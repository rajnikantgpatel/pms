<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Perfect Solusoft | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<c:url value = "/resources/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<c:url value = "/resources/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<c:url value = "/resources/css/AdminLTE.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<c:url value = "/resources/plugins/iCheck/square/blue.css"/>" rel="stylesheet" type="text/css" />
    
    <!-- fancybox -->
	<link href="<c:url value = "/resources/custom/css/jquery.fancybox.css"/>" rel="stylesheet" media="screen">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page" ng-app="mainApp">
  
  <div class="login-box" ng-controller="loginController">
      <div class="login-logo">
    <a href="#"><b>Perfect</b>Solusoft</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div ng-show="error" class="alert alert-danger">{{error}}</div>
        <form  method="POST" id="frmlogin"	name="frmlogin" ng-submit="processForm()">
          <div class="form-group has-feedback">
            <input type="email" placeholder="Username" class="form-control" placeholder="Email" id="" ng-model="formData.username" name="username" required/>
            <span ng-show = "frmlogin.username.$error.required">User name is required.</span>
            <span ng-show = "frmlogin.username.$error.email">Invalid User name.</span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" id="" ng-model="formData.password" name="password" required/>
             <span ng-show="frmlogin.password.$error.required" class="help-block">Password is required</span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <!-- <input type="checkbox"> Remember Me -->
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat" ng-disabled=" frmlogin.password.$invalid || frmlogin.username.$dirty && frmlogin.username.$invalid" >Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
        <a href="forgetpassword" class="fancybox fancybox.iframe">I forgot my password</a><br>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    

    <!-- jQuery 2.1.4 -->
    <script src="<c:url value = "/resources/plugins/jQuery/jQuery-2.1.4.min.js"/>"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<c:url value = "/resources/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<c:url value = "/resources/plugins/iCheck/icheck.min.js"/>" type="text/javascript"></script>
    
    <script src="//code.angularjs.org/1.2.20/angular.js"></script>
    <script src="//code.angularjs.org/1.2.20/angular-route.js"></script>
    <script src="//code.angularjs.org/1.2.13/angular-cookies.js"></script>
    <script src="<c:url value ="/resources/js/angular/app.js" />" type="text/javascript"></script>
    <script src="<c:url value ="/resources/js/angular/modules/controllers/logincontroller.js" />" type="text/javascript"></script>
    
    <!-- fancybox -->
    <script type="text/javascript" src="<c:url value = "/resources/custom/js/jquery.fancybox.js"/>"></script>
     <script>
	$(document).ready(function() {
		$('.fancybox').fancybox();
	});
	</script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    
  </body>
</html>