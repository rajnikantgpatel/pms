<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<html>
  <jsp:include page="head.jsp" />
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
       <jsp:include page="header.jsp" />
      <!-- Left side column. contains the sidebar -->
      <jsp:include page="left-nav.jsp" />
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <jsp:include page="body-footer.jsp" />
    </div><!-- ./wrapper -->

   <jsp:include page="footer.jsp" />
  </body>
</html>