<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<html>
  <jsp:include page="head.jsp" />
   <script>
	function forgetPassword() {
		$.blockUI({
			message : 'Please wait....',
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff'
			}
		});
		$("#frmforgetpassword").attr("action", "forgetafterpwd");
	}
</script>
 <body class="login-page bodyoverflow-y">
	<div class="login-box">
		<div class="login-box-body">
       		<p class="login-box-msg">Forget Password?</p>
        	<form name='frmforgetpassword' id="frmforgetpassword" onsubmit="forgetPassword()" action="" method="post">
				<div class="form-group has-feedback">
            		<input type="email" class="form-control" placeholder="Email" id="emailid" name="emailid" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" oninvalid="setCustomValidity('Please enter valid email address.')"/>
            		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          		</div>
          		<div class="row">
           			<div class="col-xs-4">
              			<button type="submit" class="btn btn-primary btn-block btn-flat">Continue</button>
            		</div>
          		</div>
			</form>
        </div>
	</div>	
</body>

   <jsp:include page="footer.jsp" />
</html>