<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="roleid" scope="page" value="${userInstance.userSelectedRole.id}"/>
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
          <c:if test="${roleid == 1 || roleid == 2}">
            <li>
              <a href="/PMS">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
              <!-- <ul class="treeview-menu">
                <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul> -->
            </li>
            </c:if>
            <c:if test="${roleid == 1}">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-transgender-alt"></i> <span>Manage</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li id="liusers"><a href="listusers"><i class="fa fa-user-secret"></i> Manage Users</a></li>
              </ul>
            </li>
            </c:if>
            <c:if test="${roleid == 1 || roleid == 2}">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-shopping-bag"></i> <span>Orders</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li id="liusers"><a href="listsalesorders"><i class="fa fa-shopping-basket"></i> Manage Orders</a></li>
              </ul>
            </li>
            </c:if>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>