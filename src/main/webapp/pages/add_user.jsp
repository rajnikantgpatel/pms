<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<html>
  <jsp:include page="head.jsp" />
    <script type="text/javascript">
    $(document).ready(function() {
    	$(".numberelement").keypress(function(event){
		    var key = window.event ? event.keyCode : event.which;

		    if (key == 8 || key == 46 || key == 9 || key == 37 || key == 39) {
		        return true;
		    }else if ( key < 48 || key > 57 ) {
		        return false;
		    }
		    return true;
		});
    	
    	$.ajaxSetup({
			async : true,
		});
    	loadRoleNames();
    	
    	var id = $.urlParam('id');
    	if(id != 0 && id != null && id != ""){
    		$("#email").prop("readonly", true);
    		$("#email").removeAttr('onchange');
    		$("#password").prop("readonly", true);
    		
    		$.getJSON('getuserbyid', {
    			'id' : id,
			}, function(data) {
				if (data != null && data != "") {
					$("#pagetitle").html("Edit User");
					$("#id").val(data.id);
					$("#firstname").val(data.firstName);
					$("#lastname").val(data.lastName);
					$("#mobileno").val(data.phone);
					$("#email").val(data.username);
					$("#password").val(data.password);
					var role = new Array();
					while(data.roleMasters.length >= 1){
						var roleid= data.roleMasters[(data.roleMasters.length)-1].id;
						role.push(roleid);
						data.roleMasters.length--;
					}
					$("#role").val(role);
				}
    		});
    	}
    });
    function loadRoleNames() {
		$.getJSON('getallroles', {}, function(data) {
			if (data.success) {
				var tbody = '<option value="">Select Role Name</option>';
				
				if (data.total != 0) {
					$.each(data.results, function(index, item) {
						tbody += '<option value="' +  item.id +'">'
						 	  	+ item.name;
						tbody += '</option>';
					});
				}
				$("#role").html(tbody);
			};
		});
	}
    
	function CheckEmail(callback) {
				
		$("#email").get(0).setCustomValidity("");
		var email = $("#email").val();
		if(email != null || email != ""){
			$.getJSON('getemailexist', {'email':email}, function(dataEmail) {
				console.debug("Email exist called!");
				if(!dataEmail){
					vex.dialog.alert("Email ID already exists.");
					$("#email").focus();
					callback(false);
				}else{
					callback(true);
				}
			});	
		}
	}
	function availableEmail() {
		CheckEmail(function(d){
			return d;
		});
	}
	
	function saveuser() {
		$.ajaxSetup({
			async : false
		});
			$.blockUI({
				message : 'Please wait....',
				css : {
					border : 'none',
					padding : '15px',
					backgroundColor : '#000',
					'-webkit-border-radius' : '10px',
					'-moz-border-radius' : '10px',
					opacity : .5,
					color : '#fff'
				}
			});
			$("#frmUser").attr("action", "saveuser");
	}
    </script>
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
       <jsp:include page="header.jsp" />
      <!-- Left side column. contains the sidebar -->
      <jsp:include page="left-nav.jsp" />
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       	<section class="content-header">
          		<h1 id="pagetitle">Add User</h1>
	    </section>
        <!-- Main content -->
        <section class="content">
			<div class="row smart-forms">
					<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form id="frmUser" name="frmUser"
							 action="" method="post" onsubmit="saveuser()">
							<input type="hidden" id="id" name="userid" value="0" />
							<div class="form-body">
								<div class="frm-row">
									<label class="field-label colm colm4">First Name<span
										class="required">*</span></label>
									<div class="section colm colm5">
										<input type="text" id="firstname" name="firstname"
											class="gui-input" placeholder="First Name" required>
									</div>
								</div>
								<div class="frm-row">
									<label class="field-label colm colm4">Last Name<span
										class="required">*</span></label>
									<div class="section colm colm5">
										<input id="lastname" name="lastname" type="text"
											class="gui-input" placeholder="Last Name" required/>
									</div>
								</div>
								<div class="frm-row">
									<label class="field-label colm colm4">Mobile No<span
										class="required">*</span></label>
									<div class="section colm colm5">
										<input id="mobileno" name="mobileno" type="text" maxlength ="15" required
											class="gui-input numberelement" placeholder="Mobile No"/>
									</div>
								</div>
								<div class="frm-row">
									<label class="field-label colm colm4">Email Id<span
										class="required">*</span></label>
									<div class="section colm colm5">
										<input type="email" name="email" id="email" class="gui-input"
											placeholder="abc@xyz.com" required 
											pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,10}$"
											oninvalid="setCustomValidity('Please Enter Valid Email Address.')"
											onchange = "availableEmail()"/>
									</div>
								</div>
								<div class="frm-row">
									<label class="field-label colm colm4">Password<span id="spanrequired"
										class="required">*</span></label>
									<div class="section colm colm5">
										<input type="password" name="password" id="password" class="gui-input"
											placeholder="********" required 
											pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="try{setCustomValidity('')}catch(e){}"
											oninvalid="setCustomValidity('Password must contain at least one digit/lowercase/uppercase\nletter and be at least six characters long.')"/>
									</div>
								</div>
								
								<div class="frm-row">
									<label class="field-label colm colm4">Role<span
										class="required">*</span></label>
									<div class="section colm colm4 role">
										<label class="field select"> <select id="role" name="role" required multiple>
										</select>
										</label>
									</div>
								</div>
								
								
								<div class="form-footer align-center">
									<input name="submit" class="btn btn-primary" type="submit"
									id="submit" value="Submit" /> 
									<input name="cancel" class="btn btn-primary" type="button" id="cancel"
									value="Cancel" onClick="window.history.back();" />
								</div>
							</div>
						</form>
					</section>
				</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <jsp:include page="body-footer.jsp" />
    </div><!-- ./wrapper -->

   <jsp:include page="footer.jsp" />
  </body>
</html>