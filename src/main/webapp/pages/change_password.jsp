<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<html>
  <jsp:include page="head.jsp" />
   <script>
	$(document).ready(function() {

		$('#oldpassword').bind('copy paste cut', function() {
			return false;
		});

		$('#newpassword').bind('copy paste cut', function() {
			return false;
		});

		$('#confirmnewpassword').bind('copy paste cut', function() {
			return false;
		});
	});

	function validatePassword() {
		var pass2 = document.getElementById("newpassword").value;
		var pass1 = document.getElementById("confirmnewpassword").value;
		console.log(pass2 + ">" + pass1);
		if (pass1 != null && pass1 != "" && pass2 != null && pass2 != ""
				&& pass1 != pass2) {
			confirmnewpassword.setCustomValidity("Passwords Don't Match");
		} else {
			confirmnewpassword.setCustomValidity("");
		}
	}

	function changepassword() {
		$.ajaxSetup({
			async : false,
		});

		$.blockUI({
			message : 'Please wait....',
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff'
			}
		});
		$("#frmchangepassword").attr("action", "changeafterpwd");
	}
</script>
  <body class="skin-blue sidebar-mini">
  <div class="smart-forms">
		<div class="form-header">
			<h3>Change Password</h3>
			<p>To reset your password, please provide your current password.</p>
			<p style="color: #cf2329; font-size: 15px; padding-bottom: 0px;"></p>
			<p style="color: #4D90FE; font-size: 15px; padding-bottom: 0px;"></p>
		</div>

		<form class="cf" name='frmchangepassword' id="frmchangepassword"
			onsubmit="changepassword()" action="" method="post">
			<div class="form-body">

				<div class="frm-row">

					<div class="section">
						<label for="oldpassword" class="field-label">Old Password</label>
						<label class="field prepend-icon"> <input
							name="oldpassword" id="oldpassword" class="gui-input"
							pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" placeholder="*****"
							required="" type="password"> <label for="password"
							class="field-icon"><i class="fa fa-lock"></i></label>
						</label>
					</div>

					<div class="section">
						<label for="newpassword" class="field-label">New Password</label>
						<label class="field prepend-icon"> <input
							name="newpassword" id="newpassword" class="gui-input"
							pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
							onchange="validatePassword()" placeholder="*****" required=""
							type="password"> <label for="password" class="field-icon"><i
								class="fa fa-lock"></i></label>
						</label>
					</div>

					<div class="section">
						<label for="confirmnewpassword" class="field-label">Confirm
							Password</label> <label class="field prepend-icon"> <input
							name="confirmnewpassword" id="confirmnewpassword"
							class="gui-input" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
							onchange="validatePassword()" placeholder="*****" required=""
							type="password"> <label for="password" class="field-icon"><i
								class="fa fa-lock"></i></label>
						</label>
					</div>


				</div>
			</div>

			<div class="form-footer" align="center">
				<button type="submit" class="btn btn-primary ">Change</button>
			</div>
		</form>
	</div>

   <jsp:include page="footer.jsp" />
  </body>
</html>