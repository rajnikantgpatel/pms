<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="Pages" class="com.perfectsolusoft.pms.constants.Pages" scope="application"/>
<html>
  <jsp:include page="head.jsp" />
  <jsp:include page="gride-view.jsp" />
  <script>
  $(document).ready(function() {
	  $.ajaxSetup({
			async : false,
		}); 
	  
	  $.getJSON('getallusers', {}, function(dataUsers) {
			console.debug(dataUsers.message);
			
			var admins = '';
			var sales = '';
			
			
			if (dataUsers.success && dataUsers.total != 0) {
				var count =0;
				$.each(dataUsers.results, function(indexUser, itemUser) {
					for(var i =0; i < itemUser.roleName.length; i++){
						if (itemUser.roleName[i].name == "Admin") {
							admins += '<tr><td align="center">'
							 	    + (count+= 1)
							 	    +'</td><td>'
									+ itemUser.userName
									+ '</td><td>'
									+ itemUser.emailId
									+ '</td><td>'
									+ itemUser.mobileNo
									+ '</td><td>'
									+ itemUser.roleName[i].name
									+ '</td><td align="center">'
									+ '<a title="Edit User" href="edituser?id='
									+ itemUser.user
									+ '">Edit</a>';
							if(!itemUser.isActive){
								admins += ' | <a title="Enable" href="javascript:changeuserstatus(\''+itemUser.user+'\',\''+ itemUser.emailId+'\',\''+ itemUser.roleName[i].name+'\',true);">Enable</a>';
							}else{
								admins += ' | <a title="Disable" href="javascript:changeuserstatus(\''+itemUser.user+'\',\''+ itemUser.emailId+'\',\''+ itemUser.roleName[i].name+'\',false);">Disable</a>';
							}
							admins += '</td></tr>';
									
						}else if (itemUser.roleName[i].name == "Sales") {
							sales += '<tr><td align="center">'
							 	    + (count+= 1)
							 	    +'</td><td>'
									+ itemUser.userName
									+ '</td><td>'
									+ itemUser.emailId
									+ '</td><td>'
									+ itemUser.mobileNo
									+ '</td><td>'
									+ itemUser.roleName[i].name
									+ '</td><td align="center">'
									+ '<a title="Edit User" href="edituser?id='
									+ itemUser.user
									+ '">Edit</a>';
									if(!itemUser.isActive){
										sales += ' | <a title="Enable" href="javascript:changeuserstatus(\''+itemUser.user+'\',\''+ itemUser.emailId+'\',\''+ itemUser.roleName[i].name+'\',true);">Enable</a>';
									}else{
										sales += ' | <a title="Disable" href="javascript:changeuserstatus(\''+itemUser.user+'\',\''+ itemUser.emailId+'\',\''+ itemUser.roleName[i].name+'\',false);">Disable</a>';
									}
									sales += '</td></tr>';
						}
					}
					
				});
			}
			
			$("#tbodyallusers").html(admins + sales);
			
			$("#tblallusers").dataTable();
			
			$("#tbodyadmins").html(admins);
			
			$("#tbladmins").dataTable();
			
			$("#tbodysales").html(sales);
			
			$("#tblsales").dataTable();
		});
  });
  
  function changeuserstatus(userid,username,role,status) {
	  if (userid != '') {
		  var msg = "Are you want to ";
		  msg +=   status ? " enable":" disable";
		  msg += " "+ username;
		  msg += "  As a "+role+"?";
		  vex.dialog.buttons.NO.text = 'No';
		vex.dialog.buttons.YES.text = 'Yes';
		vex.dialog.confirm({
			message : msg,
			overlayClosesOnClick: false,
			showCloseButton: false,
		    escapeButtonCloses: false,
			callback : function(value) {
				if (value) {
					$.ajax({
						url : 'changeuserstatus',
						type : 'POST',
						data : {
							userid : userid,
							type : role,
							status : status
						},
						success : function(data) {
							if (data.success) {
								vex.dialog.buttons.YES.text = 'OK';
								vex.dialog.alert({
									message : data.message,
									callback : function(value) {
										window.location.href = data.redirectUrl;
									}				
								});
							}
						}
					});
				} else {
					return false;
				}
			}
		});
	  }
	}
  </script>
  
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
       <jsp:include page="header.jsp" />
      <!-- Left side column. contains the sidebar -->
      <jsp:include page="left-nav.jsp" />
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
       	<section class="content-header">
          		<h1>Users</h1>
	    </section>
        <!-- Main content -->
        <section class="content">
			<div class="row">
	        		<div class="col-md-12">
	        			<div class="nav-tabs-custom">
	        				<div class="box-header">
								<h3 class="box-title"><a class="btn btn-primary"  href="addusers">Add User</a> </h3>
							</div>
                			<ul class="nav nav-tabs">
                  				<li class="active"><a href="#taballusers" data-toggle="tab">All Users</a></li>
                  				<li><a href="#tabadmins" data-toggle="tab">Admins</a></li>
                  				<li><a href="#tabsales" data-toggle="tab">Sales</a></li>
                			</ul>
                			
                			<div class="tab-content">
                  				<div class="tab-pane active" id="taballusers">
                  				<table id="tblallusers" class="table table-bordered table-hover">
                						<thead>
                							<tr>
                								<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</thead>
                    							
                    					<tbody id="tbodyallusers"></tbody>
                    						
                    					<tfoot style="display: none;">
	                						<tr>
    	            							<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</tfoot>
                  					</table>
                  				</div>
                  				
                  				<div class="tab-pane" id="tabadmins">
                  				<table id="tbladmins" class="table table-bordered table-hover">
                						<thead>
                							<tr>
                								<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</thead>
                    							
                    					<tbody id="tbodyadmins"></tbody>
                    						
                    					<tfoot style="display: none;">
	                						<tr>
    	            							<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</tfoot>
                  					</table>
                  				</div>
                  				
                  				<div class="tab-pane" id="tabsales">
                  				<table id="tblsales" class="table table-bordered table-hover">
                						<thead>
                							<tr>
                								<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</thead>
                    							
                    					<tbody id="tbodysales"></tbody>
                    						
                    					<tfoot style="display: none;">
	                						<tr>
    	            							<th>No.</th>
                                                <th>Name</th>
                                                <th>Email Id</th>
                                                <th>Mobile No</th>
                                                <th>Role Name</th>
                                                <th align="center">Action</th>
                							</tr>
                    					</tfoot>
                  					</table>
                  				</div>
                  			</div>                			
                		</div>
	        		</div>
	        	</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <jsp:include page="body-footer.jsp" />
    </div><!-- ./wrapper -->

   <jsp:include page="footer.jsp" />
  </body>
</html>