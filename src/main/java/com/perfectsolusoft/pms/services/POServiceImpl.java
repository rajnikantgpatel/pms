package com.perfectsolusoft.pms.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.perfectsolusoft.pms.dao.PODao;
import com.perfectsolusoft.pms.entity.PO;
import com.perfectsolusoft.pms.entity.UserMaster;


public class POServiceImpl  implements POService{
	
	@Autowired
	PODao poDao;
	
		
	@Override
	public PO save(PO po,UserMaster user) {
		if (po.getId() >0) {
			if(user != null && user.getId() > 0)
				po.setUpdatedby(user.getId());
		}else{
			po.setCreated(new Timestamp(System.currentTimeMillis()));
			if(user != null && user.getId() > 0)
				po.setCreatedby(user.getId());
		}
		po.setUpdated(new Timestamp(System.currentTimeMillis()));
		po = poDao.save(po);
		return po;
	}

	@Override
	public List<PO> findAll(String tableName) {
		List<PO> listUsers =  poDao.findAll(tableName);
		
		return listUsers;
	}

	@Override
	public PO findById(Integer id,PO po) {
		return  poDao.findById(id,po);
	}
	
}
