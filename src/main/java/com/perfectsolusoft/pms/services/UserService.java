package com.perfectsolusoft.pms.services;


import com.perfectsolusoft.pms.entity.PO;
import com.perfectsolusoft.pms.entity.UserMaster;
import com.perfectsolusoft.pms.util.Create;
import com.perfectsolusoft.pms.util.ReadList;

public interface UserService extends POService{

	public UserMaster findByEmail(String email);

	public Create saveUser(PO po,UserMaster user);
	
	public ReadList findAllUsers();
}