package com.perfectsolusoft.pms.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perfectsolusoft.pms.dao.RoleMasterDao;
import com.perfectsolusoft.pms.entity.PO;
import com.perfectsolusoft.pms.util.ReadList;


@Service("roleMasterService")
@Transactional(readOnly = true)
public class RoleMasterServiceImpl extends POServiceImpl implements RoleMasterService {

	@Autowired
	RoleMasterDao roleMasterDAO;
	
		
	@Override
	public ReadList findAll(){
		ReadList readList = new ReadList();
		List<PO> listRoleMaster = roleMasterDAO.findAll("RoleMaster");
		if (!listRoleMaster.isEmpty()) {
			readList.setResults(listRoleMaster);
			readList.setTotal(listRoleMaster.size());
		} else {
			readList.setResults(null);
			readList.setTotal(0);
		}
		readList.setSuccess(true);
		readList.setMessage("Find all completed.");
		return readList;
	}

}