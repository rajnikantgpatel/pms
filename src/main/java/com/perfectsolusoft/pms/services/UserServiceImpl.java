package com.perfectsolusoft.pms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perfectsolusoft.pms.dao.UserMasterDao;
import com.perfectsolusoft.pms.entity.PO;
import com.perfectsolusoft.pms.entity.RoleMaster;
import com.perfectsolusoft.pms.entity.UserMaster;
import com.perfectsolusoft.pms.helper.UserHelper;
import com.perfectsolusoft.pms.util.Create;
import com.perfectsolusoft.pms.util.ReadList;


@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl  extends POServiceImpl implements UserService {

	@Autowired
	UserMasterDao userMasterDao;
	
	
	@Override
	public UserMaster findByEmail(String email) {
		return userMasterDao.loadUserByUsername(email);
	}

	@Override
	public Create saveUser(PO po,UserMaster user) {
		Create create = new Create();
		UserMaster userMaster = (UserMaster) super.save(po,user);
		create.setResults(userMaster);
		create.setSuccess(true);
		create.setMessage("User Save SuccessFully.");
		return create;
	}


	
	public ReadList findAllUsers() {
		List<PO> users = super.findAll("UserMaster");
		ReadList readList = new ReadList();
		List<UserHelper> listUserHelper = new ArrayList<UserHelper>();
		
		for (PO poMaster : users) {
			UserMaster userMaster = (UserMaster) poMaster;
			List<RoleMaster> listRoleMaster =  userMaster.getRoleMasters();

			if(listRoleMaster.size() >= 2){
				
				UserHelper user = new UserHelper();

				user.setUser(userMaster.getId());
				user.setUserName(userMaster.getFirstName() + " "
						+ userMaster.getLastName());
				user.setEmailId(userMaster.getUsername());
				user.setMobileNo(userMaster.getPhone());
				user.setRoleName(listRoleMaster);
				user.setIsActive(userMaster.getIsActive());

				listUserHelper.add(user);
			} else {

				UserHelper user = new UserHelper();

				user.setUser(userMaster.getId());
				user.setUserName(userMaster.getFirstName() + " " + userMaster.getLastName());
				user.setEmailId(userMaster.getUsername());
				user.setMobileNo(userMaster.getPhone());
				user.setRoleName(listRoleMaster);
				user.setIsActive(userMaster.getIsActive());

				listUserHelper.add(user);
			}
		}
		
		if (!listUserHelper.isEmpty()) {
			readList.setResults(listUserHelper);
			readList.setTotal(listUserHelper.size());
		} else {
			readList.setResults(null);
			readList.setTotal(0);
		}

		readList.setSuccess(true);
		readList.setMessage("Find all users completed.");
		
		return readList;
	}
}