package com.perfectsolusoft.pms.services;

import java.util.List;

import com.perfectsolusoft.pms.entity.PO;
import com.perfectsolusoft.pms.entity.UserMaster;

public interface POService {

	public PO save(PO po,UserMaster user);
	
	public List<PO> findAll(String tableName);
	
	public PO findById(Integer id,PO po);
}
