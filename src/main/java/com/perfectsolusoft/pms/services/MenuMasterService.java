package com.perfectsolusoft.pms.services;

import java.util.List;

import com.perfectsolusoft.pms.entity.MenuMaster;

public interface MenuMasterService extends POService{
	
	public List<MenuMaster> findByRole(Integer role);

}
