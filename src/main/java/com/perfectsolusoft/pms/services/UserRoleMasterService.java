package com.perfectsolusoft.pms.services;

import java.util.List;

import com.perfectsolusoft.pms.entity.UserRoleMaster;

public interface UserRoleMasterService  extends POService{
	
	public List<UserRoleMaster> findByUserId(Integer userId);
	
	public UserRoleMaster findByUserIdAndRoleId(Integer userId, Integer roleId);
	
	public void DeleteUserRoleMaster(UserRoleMaster userrolemaster);

}
