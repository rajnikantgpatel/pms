package com.perfectsolusoft.pms.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("outgoingMailMasterService")
@Transactional(readOnly = true)
public class OutgoingMailMasterServiceImpl extends POServiceImpl implements OutgoingMailMasterService {

	
}
