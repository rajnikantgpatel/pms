package com.perfectsolusoft.pms.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perfectsolusoft.pms.dao.UserMasterDao;
import com.perfectsolusoft.pms.entity.RoleMaster;
import com.perfectsolusoft.pms.entity.UserMaster;
import com.perfectsolusoft.pms.security.SystemUser;


@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserMasterDao userMasterDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        try {
            UserMaster userMaster = userMasterDao.loadUserByUsername(username);

            if (userMaster == null)
                throw new UsernameNotFoundException("Credetials Invalid!");
            String password = userMaster.getPassword();

            return new SystemUser(username, password,getGrantedAuthorities(userMaster.getRoleMasters()),userMaster);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 
     * @param roles
     *            Sets of RoleMaster entities which is associated to the user
     * 
     * @return It returns the converted to lists of granted authority.
     */
    public static List<GrantedAuthority> getGrantedAuthorities(
            List<RoleMaster> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (RoleMaster role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

}
