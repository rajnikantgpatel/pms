package com.perfectsolusoft.pms.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perfectsolusoft.pms.dao.UserRoleMasterDao;
import com.perfectsolusoft.pms.entity.UserRoleMaster;

@Service("userRoleMasterService")
@Transactional(readOnly = true)
public class UserRoleMasterServiceImpl extends POServiceImpl implements UserRoleMasterService {

	@Autowired
	UserRoleMasterDao userRoleMasterDAO;
	
		
	@Override
	public List<UserRoleMaster> findByUserId(Integer userId) {
		
		return  userRoleMasterDAO.findByUserId(userId);
	}

	@Override
	public UserRoleMaster findByUserIdAndRoleId(Integer userId, Integer roleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void DeleteUserRoleMaster(UserRoleMaster userrolemaster) {
		userRoleMasterDAO.delete(userrolemaster);
	}

}
