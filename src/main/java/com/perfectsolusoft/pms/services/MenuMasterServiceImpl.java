package com.perfectsolusoft.pms.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.perfectsolusoft.pms.dao.MenuMasterDao;
import com.perfectsolusoft.pms.entity.MenuMaster;

public class MenuMasterServiceImpl extends POServiceImpl implements MenuMasterService {

	@Autowired
	MenuMasterDao menuMasterDao;
	
	@Override
	public List<MenuMaster> findByRole(Integer roleId) {
		
				
		return menuMasterDao.findMenuByRoles(roleId);
	}

}
