package com.perfectsolusoft.pms.services;

import com.perfectsolusoft.pms.util.ReadList;

public interface RoleMasterService extends POService{

	public ReadList findAll();
}