package com.perfectsolusoft.pms.helper;

import java.util.List;

public class UserHelper {

	private Integer user;
	private String userName;
	private String emailId;
	private String mobileNo;
	private List<?> roleName;
	private Boolean isActive;
	

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public List<?> getRoleName() {
		return roleName;
	}

	public void setRoleName(List<?> roleName) {
		this.roleName = roleName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}