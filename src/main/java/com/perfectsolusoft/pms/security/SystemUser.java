package com.perfectsolusoft.pms.security;


import java.util.List; 

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.perfectsolusoft.pms.entity.UserMaster;



public class SystemUser extends User {

	private static final long serialVersionUID = -3712240625427654535L;
	private UserMaster userMaster;

	private List<GrantedAuthority> authorities;
	private String email;
	

	public SystemUser(String username, String password, List<GrantedAuthority> authorities,
			UserMaster userMaster) {
		super(username,password,authorities);
		this.userMaster = userMaster;
		this.authorities = authorities;
		this.email = userMaster.getUsername();
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public List<GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	public String getEmail() {
		return email;
	}
}
