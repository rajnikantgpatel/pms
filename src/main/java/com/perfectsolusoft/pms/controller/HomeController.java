package com.perfectsolusoft.pms.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.perfectsolusoft.pms.app.SH;
import com.perfectsolusoft.pms.constants.Pages;
import com.perfectsolusoft.pms.entity.UserMaster;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session) {
		logger.info("GET:/ called!");
		
		if(session != null ){
			UserMaster userInstance = SH.getCurrentLogin(session);
			if(userInstance != null && userInstance.getUserSelectedRole() != null && userInstance.getUserSelectedRole().getId() > 0){
				return Pages.Index_Page;
			}
		}
		return Pages.Login_Page;
	}
	
	@RequestMapping(Pages.Index_Page)
	public String indexIndexPage(HttpSession session, ModelMap model) {
		logger.info("GET:/index called!");
		UserMaster userInstance = SH.getCurrentLogin(session);
		if (userInstance != null && userInstance.getUserSelectedRole() != null && userInstance.getUserSelectedRole().getId() >0) {
			return Pages.Index_Page;
		}
		return Pages.Login_Page;
	}
	
}
