package com.perfectsolusoft.pms.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.perfectsolusoft.pms.app.SH;
import com.perfectsolusoft.pms.constants.Constants;
import com.perfectsolusoft.pms.constants.Pages;
import com.perfectsolusoft.pms.entity.RoleMaster;
import com.perfectsolusoft.pms.entity.UserMaster;
import com.perfectsolusoft.pms.security.SystemUser;
import com.perfectsolusoft.pms.util.Create;

@Controller
public class LoginController {

	private Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	@Qualifier(value = "authenticationManager")
	private AuthenticationManager authenticationManager;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Locale locale, Model model,
			@RequestParam(required = false) String message) {
		logger.info("GET:/login called!");
		return Pages.Login_Page;
	}

	@RequestMapping(value = "/denied")
	public String denied() {
		return "denied";
	}

	@RequestMapping(value = "/login/failure")
	public String loginFailure(Locale locale, Model model) {
		return "redirect:"+Pages.Login_Page+"?message=Login Failed!";
	}

	@RequestMapping(value = "/login/invalid-session")
	public String invalidSession() {
		String message = "Invalid Session!";
		return "redirect:"+Pages.Login_Page+"?message=" + message;
	}
	
	@RequestMapping(value = "/logout/success")
	public String logoutSuccess() {
		String message = "Logout Success!";
		return "redirect:"+Pages.Login_Page+"?message=" + message;
	}

	@RequestMapping(value = "/login/max-sessions")
	public String maxSessions(
			Model model,
			@RequestParam(required = false, value = "message", defaultValue = "Concurrent Access limit exceeded!!") String message) {
		model.addAttribute("message", message);
		return "max_sessions";
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST , produces = "application/json")
	public @ResponseBody
	Create authenticate(ModelMap modelMap,
			 HttpSession session,
			HttpServletRequest request) {
		logger.debug("Authenticate()");
		Create create = new Create();
		try {
			
			Authentication authenticate = new UsernamePasswordAuthenticationToken(
					"username", "password");
			Authentication result = authenticationManager
					.authenticate(authenticate);

			SecurityContextHolder.getContext().setAuthentication(result);

			SystemUser systemUser = (SystemUser) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();

			UserMaster user = systemUser.getUserMaster();
			session.setAttribute(SH.USER_INSTANCE, user);
			
			List<RoleMaster> listRoles = systemUser.getUserMaster()
					.getRoleMasters();

			if(listRoles.isEmpty() || listRoles.size() > 1){
				//return "redirect:/" + Pages.Select_Role_Url;
				return create;
			}else{
				user.setUserSelectedRole(listRoles.get(0));
				//return "redirect:/" + Pages.Index_Page;
				return create;
			}
			
		} catch (Exception e) {
			logger.error("Authentication failed: " + e.getMessage());

			modelMap.put(Constants.error_msg,
					Constants.USERID_PASSWORD_WRONG);
			
			//return Pages.Login_Page;
			return create;
		}
	}
	
	@RequestMapping(value = Pages.Select_Role_Url, method = RequestMethod.GET)
	public String selectRolePage(Model model,HttpServletRequest request,HttpSession session) {
		logger.debug("GET:/selectrolepage called!");
		
		UserMaster currentUser = SH.getCurrentLogin(session);
		if(currentUser != null && currentUser.getRoleMasters() != null){
			List<RoleMaster> userRoles = currentUser.getRoleMasters();
			request.setAttribute(SH.USER_ROLES, userRoles);
			return Pages.Select_User_Role_Page;
		}
		return "redirect:/" + Pages.Login_Page;
	}
	
	@RequestMapping(value = Pages.Set_User_Role, method = RequestMethod.POST)
	public String setRole(@RequestParam(value = "role") int role, HttpSession session) {
		logger.debug("GET:/setuserrole called!");
		UserMaster userInstance = SH.getCurrentLogin(session);
		List<RoleMaster> roles = userInstance.getRoleMasters();
		for(RoleMaster userRole : roles){
			if(role == userRole.getId())
			{
				userInstance.setUserSelectedRole(userRole);
				break;
			}

		}
		return "redirect:/" + Pages.Index_Page;
	}
		
}
