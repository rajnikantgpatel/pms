package com.perfectsolusoft.pms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.perfectsolusoft.pms.app.SH;
import com.perfectsolusoft.pms.constants.Constants;
import com.perfectsolusoft.pms.constants.Pages;
import com.perfectsolusoft.pms.entity.OutgoingMailMaster;
import com.perfectsolusoft.pms.entity.RoleMaster;
import com.perfectsolusoft.pms.entity.UserMaster;
import com.perfectsolusoft.pms.entity.UserRoleMaster;
import com.perfectsolusoft.pms.services.OutgoingMailMasterService;
import com.perfectsolusoft.pms.services.RoleMasterService;
import com.perfectsolusoft.pms.services.UserRoleMasterService;
import com.perfectsolusoft.pms.services.UserService;
import com.perfectsolusoft.pms.util.Create;
import com.perfectsolusoft.pms.util.EmailUtils;
import com.perfectsolusoft.pms.util.ReadList;

@Controller
public class UserController {
	
	private Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleMasterService roleMasterService;
	
	@Autowired
	private UserRoleMasterService userRoleMasterService;
	
	@Autowired 
	private OutgoingMailMasterService outgoingMailMasterServiceImpl;
	
		
	@RequestMapping(value = Pages.User_Page_Url, method = RequestMethod.GET)
	public String listUsers() {
		logger.info("GET:/listusers called!");
		return Pages.list_users_Page;
	}
	
	@RequestMapping(value = Pages.List_Users_Mapping, method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ReadList getAllUsers(HttpSession session) {
		logger.info("GET:/getallusers called!");

		return userService.findAllUsers();
	}
	
	@RequestMapping(value = "/getallroles", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ReadList getAllRoles() {
		logger.debug("GET:/getallroles called!");
		return roleMasterService.findAll();
	}
	
	@RequestMapping(value = "/getemailexist", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	Boolean getEmailExist(
			HttpSession session,
			@RequestParam(value = "email", required = true) String email) {
		logger.info("GET:/getemailexist called!");

		try {
			UserMaster user = userService.findByEmail(email);
			if (user != null) {
				logger.debug("Email Id is exist.");
				return false;
			}else {
				logger.debug("Email Id does not exist.");
				return true;
			}
		} catch (Exception e) {
			logger.debug("Exception :" + e.getMessage());
		}
		return true;
	}
	
	@RequestMapping("/addusers")
	public String addUsers() {
		logger.info("GET:/ addusers called!");
		return Pages.Add_User_Page;
	}
	
	@RequestMapping(value = "/saveuser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	Create saveUser(
			@RequestParam(value = "userid", required = true) int userid,
			@RequestParam(value = "firstname", required = true) String firstname,
			@RequestParam(value = "lastname", required = true) String lastname,
			@RequestParam(value = "mobileno", required = true) String mobileno,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "role", required = true) String role,
			HttpServletRequest request,
			HttpSession session) {

		logger.debug("POST:/saveuser called!");
		Create create = new Create();
		try {
			UserMaster user  = userService.findByEmail(email);
				if (user != null && (user.getId() !=userid)) {
					throw new Exception(Constants.ERROR_USER_EXIST);
				}
			
			UserMaster currentUser = SH.getCurrentLogin(session);
			user = new UserMaster();
			RoleMaster roleMaster = new RoleMaster();
			UserRoleMaster userRole = new UserRoleMaster();
			
			if (userid > 0) {
				user = (UserMaster) userService.findById(userid, user);
				List<UserRoleMaster> userRoles = userRoleMasterService.findByUserId(user.getId());
				if (!"".equals(role) ) {
					String[] roleids = role.split(",");
					
					for (int i = 0; i < roleids.length; i++) {
						Boolean flag = true;
						int role_id = Integer.parseInt(roleids[i]);
						if (role_id != 0) {
							for (UserRoleMaster userRoleMasters : userRoles) {
								if (role_id == userRoleMasters.getRoleMaster().getId()) {
									flag = false;
									userRole = userRoleMasters;
									break;
								}
							}
							if (flag) {
								roleMaster = (RoleMaster) roleMasterService.findById(role_id, roleMaster);
								userRole = new UserRoleMaster();
								userRole.setUserMaster(user);
								userRole.setRoleMaster(roleMaster);
								userRole.setIsActive(true);
								userRoleMasterService.save(userRole, currentUser);

							} else {
								userRoles.remove(userRole);
							}
						}
					}
				}
				for (UserRoleMaster userRoleMasters : userRoles) {
					userRoleMasterService.DeleteUserRoleMaster(userRoleMasters);
				}
				user.setFirstName(firstname);
				user.setLastName(lastname);
				user.setPhone(mobileno);
				create = userService.saveUser(user, currentUser);
				
				user = (UserMaster) create.getResults();
			} else {
				user.setFirstName(firstname);
				user.setLastName(lastname);
				user.setPhone(mobileno);
				user.setIsActive(true);
				user.setPassword(password);
				user.setUsername(email);
				create = userService.saveUser(user, currentUser);
				user = (UserMaster) create.getResults();
				if (!"".equals(role)) {
					String[] roleids = role.split(",");
					for (int i = 0; i < roleids.length; i++) {
						int role_id = Integer.parseInt(roleids[i]);
						if (role_id != 0) {
							roleMaster = (RoleMaster) roleMasterService.findById(role_id, roleMaster);
							userRole = new UserRoleMaster();
							userRole.setUserMaster(user);
							userRole.setRoleMaster(roleMaster);
							userRole.setIsActive(true);
							userRoleMasterService.save(userRole, currentUser);
						}
					}
				}
			}
			
			create.setMessage("User saved successfully.");
			create.setRedirectUrl(Pages.User_Page_Url);
		} catch (Exception ex) {
			logger.debug("Exception : " + ex.toString());
			create.setSuccess(false);
			create.setResults(null);
			create.setMessage("User cannot be saved.");
			if(ex.getMessage() != null && (Constants.ERROR_USER_EXIST).equalsIgnoreCase(ex.getMessage()))
				create.setMessage(Constants.ERROR_USER_EXIST);
		}

		logger.debug("Status : " + create.getSuccess());
		return create;
	}
	
	@RequestMapping(value = Pages.Edit_User_Mapping, method = RequestMethod.GET)
	public String editUser(@RequestParam(value = "id", required = true) String id) {
		logger.debug("GET:/edituser called!");
		logger.debug("ID:::::  " + id);
		return Pages.Add_User_Page;
	}
	
	@RequestMapping(value = Pages.Get_User_By_Id, method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	UserMaster getUserById(Integer id) {
		logger.debug("GET:/getuserbyid called!");

		UserMaster user = (UserMaster) userService.findById(id, new UserMaster());
		return user;
	}
	
	@RequestMapping(value = Pages.Status_Change_Url, method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	Create changeUserStatus(Integer userid, String type, boolean status, HttpSession session) {
		logger.debug("GET:/changeuserstatus called!");
		logger.debug("ID:::::  " + userid);
		String statusvalue;
		if (status) {
			statusvalue = "Enabled";
		} else {
			statusvalue = "Disabled";
		}
		Create create = new Create();
		try {
			UserMaster currentUser = SH.getCurrentLogin(session);
			UserMaster user = (UserMaster) userService.findById(userid, new UserMaster());
			user.setIsActive(status);
			userService.saveUser(user,currentUser);
			
			create.setMessage(type +" " + statusvalue + " successfully.");
			create.setSuccess(true);
			create.setRedirectUrl(Pages.User_Page_Url);
		} catch (Exception e) {
			logger.debug("Exception : " + e.toString());
		}
		return create;
	}
	
	@RequestMapping(value = Pages.Change_User_Password, method = RequestMethod.GET, produces = "application/json")
	public String changePasswordPage() {
		logger.debug("GET:/changepassword called!");
		return Pages.Change_User_Password_Page;
	}

	@RequestMapping(value = Pages.Change_After_password, method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	Create changePassword(
			@RequestParam(value = "oldpassword") String oldPassword,
			@RequestParam(value = "newpassword") String newPassword,
			HttpSession session) {
		logger.debug("POST:/changepwd called!");
		Create create = new Create();

		UserMaster user = SH.getCurrentLogin(session);
		try {
			if (user.getPassword().equals(oldPassword)) {
				user.setPassword(newPassword);
				create = userService.saveUser(user, user);
				create.setMessage(Constants.PASSWORD_CHANGE_SUCCESS_MSG);
				create.setSuccess(true);
			} else {
				create.setMessage(Constants.OLD_PASSWORD_WRONG_MSG);
				create.setSuccess(false);

			}
		} catch (Exception e) {
			logger.debug("Exception=" + e);
			create.setMessage(Constants.PASSWORD_CHANGE_ERROR_MSG);
			create.setSuccess(false);
		}
		return create;
	}
	
	@RequestMapping(value = Pages.Forget_Before_password, method = RequestMethod.GET)
	public String forgetPasswordPage() {
		logger.debug("GET:/forgetpassword called!");
		return Pages.Forget_Password_Page;
	}
	
	@RequestMapping(value = Pages.Forget_After_password, method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	Create forgetPassword(@RequestParam(value = "emailid") String email,
			HttpServletRequest request) {
		logger.debug("POST:/forgetafterpwd called!");
		Create create = new Create();

		try {
			UserMaster user = userService.findByEmail(email);
			if (user == null) {
				throw new Exception(Constants.ERROR_USER_EXIST);
			}
			String password = RandomStringUtils.randomAlphanumeric(10);
			user.setPassword(password);
			userService.saveUser(user, user);
			
			String url = request.getRequestURL().toString();
			String baseURL = url.substring(0, url.length()
					- request.getRequestURI().length())
					+ request.getContextPath();
			
			//baseURL = baseURL.substring(0, baseURL.lastIndexOf("/"));
			
			StringBuilder mailContent = new StringBuilder();
			mailContent.append("<h3><strong>Dear ");
			mailContent.append(user.getFirstName());
			mailContent.append(" ");
			mailContent.append(user.getLastName());
			mailContent.append(",</strong></h3><p>You have requested to change your password, Your new Password Is:");
			mailContent.append(password);
			mailContent.append("</p><br/><p> Now You Can login by clicking on the link below:</p><p> <a href=\"");
			mailContent.append(baseURL);
			mailContent.append("\" target=\"_blank\"> </p><p>Once you login. Please, Change the password.</p>");
						
			
			OutgoingMailMaster outgoingMailMaster = new OutgoingMailMaster();
			outgoingMailMaster.setTo(user.getUsername());
			outgoingMailMaster.setCc("");
			outgoingMailMaster.setBcc("");
			outgoingMailMaster.setSubject("Password change request");
			outgoingMailMaster.setContent(mailContent.toString());
			outgoingMailMaster.setIsActive(true);
			outgoingMailMaster.setIsSent(false);
			outgoingMailMasterServiceImpl.save(outgoingMailMaster, user);
			boolean success =EmailUtils.sendMailInstantly(outgoingMailMaster);
			if(success){
				create.setMessage(Constants.FORGET_PASSWORD_MAIL_SENT_SUCCESS_MSG);
				outgoingMailMaster.setIsSent(true);
				outgoingMailMasterServiceImpl.save(outgoingMailMaster, user);
			}else
				create.setMessage(Constants.FORGET_PASSWORD_MAIL_SENT_ERROR_MSG);
			create.setResults("");
			create.setSuccess(true);

			logger.debug("Status : true");
		} catch (Exception ex) {
			create.setMessage(email + " does not exist. Please try another.");
			create.setResults("");
			create.setSuccess(false);
		}
		return create;
	}
	
}
