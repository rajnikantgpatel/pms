package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "role_master")
@AttributeOverride( name="created", column = @Column(name="created") )
public class RoleMaster extends PO {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	

	/** default constructor */
    public RoleMaster() {
    }
    
    public RoleMaster(int id,Timestamp created){
    	super(id,created);
    }


	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}