package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "menu_role_mapping")
@AttributeOverride( name="created", column = @Column(name="created"))
public class MenuRoleMaster extends PO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuMaster menuMaster;
	private RoleMaster roleMaster;
	
	
	/** default constructor */
	public MenuRoleMaster() {
	}

	/** minimal constructor */
	public MenuRoleMaster(MenuMaster menuMaster, RoleMaster roleMaster) {
		this.menuMaster = menuMaster;
		this.roleMaster = roleMaster;
		
	}
	
	public MenuRoleMaster(int id,Timestamp created){
    	super(id,created);
    }
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menu_id")
	public MenuMaster getMenuMaster() {
		return menuMaster;
	}

	public void setMenuMaster(MenuMaster menuMaster) {
		this.menuMaster = menuMaster;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	public RoleMaster getRoleMaster() {
		return roleMaster;
	}

	public void setRoleMaster(RoleMaster roleMaster) {
		this.roleMaster = roleMaster;
	}
}
