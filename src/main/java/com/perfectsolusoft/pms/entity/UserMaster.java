package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name = "user_master")
@Inheritance(strategy = InheritanceType.JOINED)
@AttributeOverride( name="created", column = @Column(name="created") )
public class UserMaster extends PO {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String phone;
    private List<RoleMaster> roleMasters = new ArrayList<RoleMaster>(0);
    private RoleMaster userSelectedRole ;
    
    // Constructors
    /** default constructor */
    public UserMaster() {
    }
    
    public UserMaster(int id,Timestamp created){
    	super(id,created);
    }
    
    /** minimal constructor */
	public UserMaster(String username, String password) {
        this.username = username;
        this.password = password;
       
    }

    /** full constructor */
    public UserMaster(String username, String password, List<RoleMaster> roleMasters) {
        this.username = username;
        this.password = password;
        this.roleMasters = roleMasters;

    }

    @Column(name = "username", nullable = false, length = 64, unique = true)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length = 45)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
   
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role_mapping", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
    @Fetch(FetchMode.SUBSELECT)
    public List<RoleMaster> getRoleMasters() {
        return this.roleMasters;
    }

    public void setRoleMasters(List<RoleMaster> roleMasters) {
        this.roleMasters = roleMasters;
    }
    @Column(name = "firstName", length = 25)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "lastName", length = 25)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(name = "phone", length = 15)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Transient
	public RoleMaster getUserSelectedRole() {
		return userSelectedRole;
	}

	public void setUserSelectedRole(RoleMaster userSelectedRole) {
		this.userSelectedRole = userSelectedRole;
	}
    
    
}