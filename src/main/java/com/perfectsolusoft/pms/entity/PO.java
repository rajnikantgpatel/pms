package com.perfectsolusoft.pms.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class PO implements Serializable{
	 	/**
	 * 
	 */
	private static final long serialVersionUID = -8653459411611052681L;
		private Timestamp created;
		private Timestamp updated;
	    private int createdby;
	    private int updatedby;
	    private Boolean isActive;
	    private int id;
	    
	    
	    public PO() {
		}
		public PO(int id, Timestamp createDate) {
			this.id = id;
			this.created = createDate;
		}
	   	
		@Id
	    @GeneratedValue(strategy = IDENTITY)
	    @Column(name = "id", unique = true, nullable = false)
	    public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		@Column(name = "isactive")
		public Boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(Boolean isactive) {
			this.isActive = isactive;
		}
		@Column(name = "created")
		public Timestamp getCreated() {
			return created;
		}
		public void setCreated(Timestamp created) {
			this.created = created;
		}
		@Column(name = "updated")
		public Timestamp getUpdated() {
			return updated;
		}
		public void setUpdated(Timestamp updated) {
			this.updated = updated;
		}
		
		@Column(name = "createdby")
		public int getCreatedby() {
			return createdby;
		}
		public void setCreatedby(int createdby) {
			this.createdby = createdby;
		}
		@Column(name = "updatedby")
		public int getUpdatedby() {
			return updatedby;
		}
		public void setUpdatedby(int updatedby) {
			this.updatedby = updatedby;
		}
	    
}
