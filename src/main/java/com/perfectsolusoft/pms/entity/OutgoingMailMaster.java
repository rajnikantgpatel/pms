package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "outgoing_mail_master")
@AttributeOverride( name="created", column = @Column(name="created"))
public class OutgoingMailMaster extends PO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String to ;
	private String cc ;
	private String bcc ;
	private String subject;
	private String content;
	private boolean isSent;
	
	/** default constructor */
    public OutgoingMailMaster() {
    }
    
    public OutgoingMailMaster(int id,Timestamp created){
    	super(id,created);
    }

    @Column(name="mail_to")
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	@Column(name="mail_cc")
	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	@Column(name="mail_bcc")
	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	@Column(name="subject")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name="content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name="issent")
	public boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(boolean isSent) {
		this.isSent = isSent;
	}
	
	

}
