package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "role_master")
public class AppInfoMaster extends PO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name ;
	private String value;
	
	/** default constructor */
    public AppInfoMaster() {
    }
    
    public AppInfoMaster(int id,Timestamp created){
    	super(id,created);
    }
	
	@Transient
	public int getId() {
		return super.getId();
	}
	@Transient
	public Boolean getIsActive() {
		return super.getIsActive();
	}
	@Transient
	public Timestamp getCreated() {
		return super.getCreated();
	}
	@Transient
	public Timestamp getUpdated() {
		return super.getUpdated();
	}
	@Transient
	public int getCreatedby() {
		return super.getCreatedby();
	}
	@Transient
	public int getUpdatedby() {
		return super.getUpdatedby();
	}
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "value")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	

	
}
