package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@SuppressWarnings("serial")
@Entity
@Table(name = "user_role_mapping")
@AttributeOverride( name="created", column = @Column(name="created") )
public class UserRoleMaster extends PO{
	
	private UserMaster usermaster;
	private RoleMaster rolemaster;
	
	/** default constructor */
	public UserRoleMaster() {
	}

	/** minimal constructor */
	public UserRoleMaster(UserMaster usermaster, RoleMaster rolemaster) {
		this.usermaster = usermaster;
		this.rolemaster = rolemaster;
		
	}
	
	public UserRoleMaster(int id,Timestamp created){
    	super(id,created);
    }
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	public UserMaster getUserMaster() {
		return usermaster;
	}

	public void setUserMaster(UserMaster usermaster) {
		this.usermaster = usermaster;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	public RoleMaster getRoleMaster() {
		return rolemaster;
	}

	public void setRoleMaster(RoleMaster rolemaster) {
		this.rolemaster = rolemaster;
	}

}
