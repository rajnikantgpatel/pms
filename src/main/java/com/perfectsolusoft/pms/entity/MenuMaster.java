package com.perfectsolusoft.pms.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "menu_master")
@AttributeOverride( name="created", column = @Column(name="created"))
public class MenuMaster extends PO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int sequance ;
	private String name;
	private String link;
	private MenuMaster parentMenuMaster;
	private List<RoleMaster> roleMasters = new ArrayList<RoleMaster>(0);
	private String css;

	/** default constructor */
    public MenuMaster() {
    }
    
    public MenuMaster(int id,Timestamp created){
    	super(id,created);
    }

    @Column(name="sequance")
	public int getSequance() {
		return sequance;
	}

	public void setSequance(int sequance) {
		this.sequance = sequance;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="link")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@ManyToOne(cascade={CascadeType.ALL},fetch = FetchType.LAZY)
    @JoinColumn(name="parent_id")
	public MenuMaster getParentId() {
		return parentMenuMaster;
	}

	public void setParentId(MenuMaster parentMenuMaster) {
		this.parentMenuMaster = parentMenuMaster;
	}
	
	@Column(name="css")
	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}
    
	 @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	 @JoinTable(name = "menu_role_mapping", joinColumns = { @JoinColumn(name = "menu_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
	 @Fetch(FetchMode.SUBSELECT)
	 public List<RoleMaster> getRoleMasters() {
	     return this.roleMasters;
	 }

	 public void setRoleMasters(List<RoleMaster> roleMasters) {
	     this.roleMasters = roleMasters;
	 }
    
}
