package com.perfectsolusoft.pms.constants;

public class Constants {

	
	public static final String USER_NAME = "username";
	public static final String USER_ROLE = "userrole";
	public static final String USER_ID = "userid";
	public static final String error_msg = "errormsg";
	
	/* file path */
	
	public static final String APP_INFO_PATH = "/appconfig/";
	
	
	/* constant massage */
	public static final String ERROR_USER_EXIST = "Email ID already exists.";
	public static final String USERID_PASSWORD_WRONG = "E-mail address or Password may be wrong. Please try again.";
	public static final String PASSWORD_CHANGE_SUCCESS_MSG = "Your Password has been changed successfully.";
	public static final String OLD_PASSWORD_WRONG_MSG ="Old Password may be wrong. Please try again.";
	public static final String PASSWORD_CHANGE_ERROR_MSG = "Your Password cannot be changed.";
	public static final String FORGET_PASSWORD_MAIL_SENT_SUCCESS_MSG = "Recovery link has been sent to your email address.";
	public static final String FORGET_PASSWORD_MAIL_SENT_ERROR_MSG = "Something went wrong, Please contact Admin. ";
}