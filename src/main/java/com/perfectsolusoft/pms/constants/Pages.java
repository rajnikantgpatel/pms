package com.perfectsolusoft.pms.constants;

public class Pages {
	public static final String Login_Page = "login";
	public static final String Index_Page = "index";
	public static final String Add_User_Page = "add_user";
	public static final String list_users_Page = "list_users";
	public static final String Select_User_Role_Page = "select_user_role";
	public static final String Change_User_Password_Page = "change_password";
	public static final String Forget_Password_Page = "forget_password";
	
	
	/* mapping */
	public static final String List_Users_Mapping = "getallusers";
	public static final String Edit_User_Mapping = "edituser";
	public static final String Get_User_By_Id = "getuserbyid";
	public static final String User_Page_Url = "listusers";
	public static final String Status_Change_Url = "changeuserstatus";
	public static final String Select_Role_Url = "selectrolepage";
	public static final String Set_User_Role = "setuserrole";
	public static final String Change_User_Password = "changepassword";
	public static final String Change_After_password = "changeafterpwd";
	public static final String Forget_Before_password = "forgetpassword";
	public static final String Forget_After_password = "forgetafterpwd";
	
	 	
}
