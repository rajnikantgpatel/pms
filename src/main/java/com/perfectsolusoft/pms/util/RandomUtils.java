package com.perfectsolusoft.pms.util;

import java.security.SecureRandom;

public class RandomUtils {


	public static SecureRandom rnd = new SecureRandom();
	public static final String CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static String generateToken(Integer length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++)
			sb.append(CHARACTERS.charAt(rnd.nextInt(CHARACTERS.length())));
		return sb.toString();
	}
}
