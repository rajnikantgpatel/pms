package com.perfectsolusoft.pms.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.perfectsolusoft.pms.app.CTX;

public class Environment {

	public static boolean isLocal() {
		return "true".equals(lookupStringValue("dev")) ? true : false;
	}
	
	public static String lookupStringValue(String varName) {
		String retValue = null;
		try {
			Context initContext = new InitialContext();
			Context env = (Context) initContext.lookup("java:/comp/env");
			Object value = env.lookup(varName);
			if (value instanceof String)
				retValue = (String) value;
			else if (null != value)
				retValue = value.toString();
		} catch (NamingException ne) {
		}
		return retValue;
	}
	
	public static String getAppInfo(String key) {
		String retValue = (String) CTX.getAppInfo().get(key);
		return retValue;
	}

}
