package com.perfectsolusoft.pms.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.perfectsolusoft.pms.entity.OutgoingMailMaster;

public class EmailUtils {
	
	private static Logger logger = LoggerFactory.getLogger(EmailUtils.class);
	
	public static boolean sendMailInstantly(OutgoingMailMaster outgoingMailMaster){
		boolean isSent = false;
		try {
			Session session = getConfig();
			Message mailMessage = new MimeMessage(session);
			mailMessage.setFrom(new InternetAddress(Environment.getAppInfo("SMTP_EMAILID")));
			if (!StringUtils.isEmpty(outgoingMailMaster.getTo()))
				mailMessage.setRecipients(Message.RecipientType.TO,
						StringUtils.covertToInternetAddress(outgoingMailMaster.getTo(), ","));
			if (!StringUtils.isEmpty(outgoingMailMaster.getCc()))
				mailMessage.setRecipients(Message.RecipientType.CC,
						StringUtils.covertToInternetAddress(outgoingMailMaster.getCc(), ","));
			if (!StringUtils.isEmpty(outgoingMailMaster.getBcc()))
				mailMessage.setRecipients(Message.RecipientType.BCC,
						StringUtils.covertToInternetAddress(outgoingMailMaster.getBcc(), ","));
			mailMessage.setSubject(outgoingMailMaster.getSubject());
			mailMessage.setSentDate(new Date());
			mailMessage.setContent(outgoingMailMaster.getContent(), "text/html");
			mailMessage.saveChanges();
			Transport.send(mailMessage);
			isSent = true;
			logger.debug("mail sent" + outgoingMailMaster.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSent;
	}

	private static Session getConfig() {
		final String host = Environment.getAppInfo("SMTP_HOST");
		final String smtpPort = Environment.getAppInfo("SMTP_PORT");
		final String fromEmail = Environment.getAppInfo("SMTP_EMAILID");
		final String password = Environment.getAppInfo("SMTP_PASSWORD");

		String sslFactory = "javax.net.ssl.SSLSocketFactory";

		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.put("mail.smtp.port", smtpPort);
		if (host.contains("gmail")) {
			props.put("mail.smtp.socketFactory.port", smtpPort);
			props.put("mail.smtp.socketFactory.class", sslFactory);
			props.put("mail.smtp.socketFactory.fallback", "false");
		}

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		return session;
	}
}
