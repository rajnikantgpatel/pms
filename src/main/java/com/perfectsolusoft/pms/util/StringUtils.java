package com.perfectsolusoft.pms.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class StringUtils {
	
	public static InternetAddress[] covertToInternetAddress(String value, String regex) throws AddressException {
		InternetAddress[] inetArrEmail;
		if (isEmpty(value)) {
			inetArrEmail = new InternetAddress[1];
			inetArrEmail[0] = new InternetAddress();
		} else {
			String[] strArrEmail = value.split(regex);
			inetArrEmail = new InternetAddress[strArrEmail.length];
			for (int i = 0; i < strArrEmail.length; i++) {
				inetArrEmail[i] = new InternetAddress(strArrEmail[i]);
			}
		}
		return inetArrEmail;
	}

	public static String convertFromInternetAddress(InternetAddress[] value,String regex){
		String strEmail ="";
		for(int i = 0 ; i<value.length;i++){
			strEmail +=value[i];
		}
		return strEmail;
	}
	
	public static boolean isEmpty(String value){
		if(value == null || value == ""){
			return true;
		}
		return false;
	}
}
