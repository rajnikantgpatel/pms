package com.perfectsolusoft.pms.util;

import com.perfectsolusoft.pms.entity.UserMaster;

public class UserRoleUtils {
	
	public static final int ROLE_ADMIN = 1;
	
	public static final int ROLE_SALES =2;
	
	public static boolean isAdmin(UserMaster currentUser){
		return (currentUser.getUserSelectedRole().getId() & ROLE_ADMIN ) !=0 ;
	}
	
	public static boolean isSales(UserMaster currentUser){
		return (currentUser.getUserSelectedRole().getId() & ROLE_SALES ) !=0 ;
	}

}
