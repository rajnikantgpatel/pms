package com.perfectsolusoft.pms.util;

import java.util.List;
import java.util.Map;

public class ReadList {
	private List<?> results;
    private boolean success;
    private Number total;
    private String message;
    private Map<?,?> mapResults;
    private List<?> data;
    private Number recordsTotal;
    private Number recordsFiltered;
    
	public List<?> getResults() {
		return results;
	}
	
	public void setResults(List<?> results) {
		this.results = results;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public void setTotal(Number total) {
		this.total = total;
	}

	public Number getTotal() {
		return total;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Map<?, ?> getMapResults() {
		return mapResults;
	}

	public void setMapResults(Map<?, ?> mapResults) {
		this.mapResults = mapResults;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public Number getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(Number recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public Number getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(Number recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
	
	
	
}