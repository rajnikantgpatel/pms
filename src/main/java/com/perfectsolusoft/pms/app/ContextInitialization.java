package com.perfectsolusoft.pms.app;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.web.WebApplicationInitializer;

import com.perfectsolusoft.pms.constants.Constants;
import com.perfectsolusoft.pms.util.Environment;

public class ContextInitialization implements WebApplicationInitializer  {
	private final Logger log = Logger.getLogger(ContextInitialization.class);


	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		URL url = ContextInitialization.class.getResource("/log4j.properties");
		if (url != null && "jar".equals(url.getProtocol())) {
			url = null;// ignore log4j from any jar on classpath.
		}
		if (url == null) {
			Properties props = new Properties();
			props.put("log4j.rootCategory", "info, console");
			props.put("log4j.appender.console", "org.apache.log4j.ConsoleAppender");
			props.put("log4j.appender.console.layout", "org.apache.log4j.PatternLayout");
			props.put("log4j.appender.console.layout.ConversionPattern", "%-4r [%t] %-5p %c %x - %m%n");
			props.put("log4j.appender.stdout.Target", "System.out");
			props.put("log4j.logger.org.exolab.castor", "info,console");
			// props.put("log4j.logger.net.sourceforge.jtds", "trace,console");
			// props.put("log4j.logger.org.apache.axis2","debug,console");
			if (Environment.isLocal())
				props.put("log4j.logger.httpclient.wire", "debug,console");

			PropertyConfigurator.configure(props);
		}
		
		log.info("=========================Perfect Solusoft initialization Begin for WebApp: " + servletContext.getContextPath() + " =================================");			      
		
		CTX.setServletContext(servletContext);
		servletContext.setAttribute("CONTEXT_PATH", servletContext.getContextPath());
		
		registerAppInfo(servletContext);
		
		log.info("=========================Perfect Solusoft initialization End=================================");
	}
	
	
	void registerAppInfo(ServletContext sc) {

		Properties prop = new Properties();
		
		try {
			File fileAppInfo = new File(sc.getRealPath(Constants.APP_INFO_PATH + File.separator + "app_info.properties"));
			InputStream inAppInfo = new FileInputStream(fileAppInfo);
			if (inAppInfo != null) {
				prop.load(inAppInfo);
			}
			sc.setAttribute("APP_INFO", prop);
			log.info("App Info loaded");
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
}
