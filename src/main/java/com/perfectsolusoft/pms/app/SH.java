package com.perfectsolusoft.pms.app;

import javax.servlet.http.HttpSession;

import com.perfectsolusoft.pms.entity.UserMaster;

public class SH {
	public static final String USER_INSTANCE = "userInstance";
	public static final String USER_ROLES = "userroles";
	
	public static UserMaster getCurrentLogin(HttpSession session) {
		return (UserMaster) session.getAttribute(USER_INSTANCE);
	}

}
