package com.perfectsolusoft.pms.app;

import java.util.Properties;

import javax.servlet.ServletContext;

public class CTX {
	
	private static ServletContext servletContext;

	public static void setServletContext(ServletContext servletContext) {
		if (CTX.servletContext != null)
			throw new RuntimeException("ServletContext is already set. Cannot be set again");
		CTX.servletContext = servletContext;
	}

	public static ServletContext getServletContext() {
		return servletContext;
	}
	
	public static Properties getAppInfo() {
		return (Properties) getServletContext().getAttribute("APP_INFO");
	}
	
	public static void setServletAttribute(String name,Object value){
		getServletContext().setAttribute(name, value);
		
	}

}
