package com.perfectsolusoft.pms.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.perfectsolusoft.pms.entity.UserRoleMaster;

@Component("userRoleMasterDao")
public class UserRoleMasterDaoImpl extends PODaoImpl implements UserRoleMasterDao{

	/*UserRoleMasterDaoImpl(DataSource dataSource) {
			super(dataSource);
			// TODO Auto-generated constructor stub
		}
	    
	UserRoleMasterDaoImpl(){
	    	
	    }*/
	
	@Override
	@SuppressWarnings("unchecked")
	public List<UserRoleMaster> findByUserId(Integer userId) {
		log.debug("getting userRoleMaster instances with user_id : " + userId);
		try {
			String queryString = "from UserRoleMaster as model where model.userMaster = "
					+ userId;
			Query queryObject = getEntityManager().createQuery(queryString);
			return queryObject.getResultList();
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

}
