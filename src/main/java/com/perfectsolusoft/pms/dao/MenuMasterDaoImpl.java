package com.perfectsolusoft.pms.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.perfectsolusoft.pms.entity.MenuMaster;

public class MenuMasterDaoImpl  extends PODaoImpl implements MenuMasterDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<MenuMaster> findMenuByRoles(Integer roleId) {
			Session session = getEntityManager().unwrap(Session.class);
	        Criteria criteria = session.createCriteria(MenuMaster.class);
	        criteria.createAlias("roleMasters", "rm", Criteria.LEFT_JOIN);
	        criteria.add(Restrictions.eq("rm.id", roleId));
	        // criteria.add(Restrictions.eq("enabled", true));
	        return criteria.list();
	}

}
