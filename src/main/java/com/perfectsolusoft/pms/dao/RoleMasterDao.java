package com.perfectsolusoft.pms.dao;

import java.util.List;

import com.perfectsolusoft.pms.entity.PO;

public interface RoleMasterDao {
	public PO save(PO entity);
	
	public void delete(PO entity);

	public PO update(PO entity);
	
	public PO findById(Integer id,PO po);
	
	public List<PO> findByProperty(String Table,String propertyName, Object value);

    public List<PO> findAll(String Table);
}
