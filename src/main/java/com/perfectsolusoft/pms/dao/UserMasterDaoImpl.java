package com.perfectsolusoft.pms.dao;


import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.perfectsolusoft.pms.entity.UserMaster;

@Component("userMasterDao")
public class UserMasterDaoImpl extends PODaoImpl implements UserMasterDao {

   
    /*UserMasterDaoImpl(DataSource dataSource) {
		super(dataSource);
		// TODO Auto-generated constructor stub
	}
    
    UserMasterDaoImpl(){
    	
    }*/

	@Override
    public List<UserMaster> findAllUserByUserNamePattern(String username) {

        try {
            TypedQuery<UserMaster> query = getEntityManager().createNamedQuery(
                    "getUserMasterByUsernamePattern", UserMaster.class);
            query.setParameter("username", username);
            return query.getResultList();
        } catch (RuntimeException re) {
            log.error("find all failed" + re);
            throw re;
        }
    }

    @Override
    public UserMaster findUserByUsername(String username) {
        try {
            TypedQuery<UserMaster> query = getEntityManager().createNamedQuery(
                    "getUserMasterByUsername", UserMaster.class);
            query.setParameter("username", username);

            List<UserMaster> userMasters = query.getResultList();
            if (userMasters.size() > 0)
                return userMasters.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            log.error("find all failed" + re);
            throw re;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UserMaster> findUsersByRoles(String role) {
        Session session = getEntityManager().unwrap(Session.class);
        Criteria criteria = session.createCriteria(UserMaster.class);
        criteria.createAlias("roleMasters", "rm", Criteria.LEFT_JOIN);
        criteria.add(Restrictions.eq("rm.authority", role));
        // criteria.add(Restrictions.eq("enabled", true));
        return criteria.list();
    }

    @Override
    // @Transactional(readOnly=true)
    public UserMaster loadUserByUsername(String username) {
        Session session = getEntityManager().unwrap(Session.class);
        Criteria criteria = session.createCriteria(UserMaster.class);
        criteria.add(Restrictions.eq("username", username));
        return (UserMaster) criteria.uniqueResult();
    }

}