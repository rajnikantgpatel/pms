package com.perfectsolusoft.pms.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.perfectsolusoft.pms.entity.PO;

@Component
@Transactional(readOnly = true)
public class PODaoImpl{
	
	 @PersistenceContext
	 protected EntityManager em;

	 Logger log = Logger.getLogger(this.getClass());

	 protected EntityManager getEntityManager() {
	      return em;
	 }
	 
	 
	 /*PODaoImpl(){
	        
	 }
	 
	 @Autowired
	 PODaoImpl(DataSource dataSource){
	        setDataSource(dataSource);
	    }*/
	 
	 @Transactional(propagation = Propagation.REQUIRED)
	public PO save(PO entity) {
		try {
			entity =getEntityManager().merge(entity);
			getEntityManager().flush();
			return entity;
		} catch (RuntimeException re) {
	            log.error("save failed" + re);
	            throw re;
	        }
	}
	public void delete(PO entity) {
		 try {
	            entity = getEntityManager().getReference(entity.getClass(),
	                    entity.getId());
	            getEntityManager().remove(entity);
	            getEntityManager().flush();
	        } catch (RuntimeException re) {
	            log.error("delete failed" + re);
	            throw re;
	        }
	}
	public PO update(PO entity) {
		try {
            PO result = getEntityManager().merge(entity);
            return result;
        } catch (RuntimeException re) {
            log.error("update failed" + re);
            throw re;
        }
	}
	public PO findById(Integer id,PO po) {
		try {
            PO instance = (PO) getEntityManager().find(po.getClass(), id);
            return instance;
        } catch (RuntimeException re) {
            log.error("find failed" + re);
            throw re;
        }
	}
	@SuppressWarnings("unchecked")
	public List<PO> findByProperty(String Table,String propertyName, Object value) {
		try {
            final String queryString = "select model from "+ Table +" model where model."
                    + propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);
            return query.getResultList();
        } catch (RuntimeException re) {
            log.error("find by property name failed" + re);
            throw re;
        }
	}
	@SuppressWarnings("unchecked")
	public List<PO> findAll(String table) {
		try {
            final String queryString = "select model from "+ table +"  model";
            Query query = getEntityManager().createQuery(queryString);
            return query.getResultList();
        } catch (RuntimeException re) {
            log.error("find all failed" + re);
            throw re;
        }
	}

}
